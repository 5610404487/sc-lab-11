package HashMap;
import java.util.*;

public class WordCounter {
	String message;
	HashMap<String,Integer> wordCount;
	public WordCounter(String message){
		this.message = message;
		wordCount = new HashMap<String,Integer>();
	}
	public void count(){
		String[] splitMessage = message.split(" ");
		for (String s : splitMessage){
			if(wordCount.containsKey(s)){
				wordCount.put(s, wordCount.get(s)+1);
			}
			else{
				wordCount.put(s, 1);
			}
		}
	}
	public int hasWord(String word){
		if(wordCount.containsKey(word)){
			return wordCount.get(word);
		}
		else{
			return 0;
		}	
	}
}
