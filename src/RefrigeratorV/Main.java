package RefrigeratorV;

public class Main {
	public static void main(String[] args) {
		Refrigerator things = new Refrigerator(5);
		try {
			things.put("cabbage");
			things.put("cucumber");
			things.put("mushroom");
			things.put("pumpkin");
			things.put("tomato");
			System.out.println(things.takeOut("mushroom"));
			things.put("lettuce");
		}
		catch (FullException e) {
			System.err.println("Full O_O!!");
		}
		finally{
			System.out.println(things.toString());
		}
	}
}