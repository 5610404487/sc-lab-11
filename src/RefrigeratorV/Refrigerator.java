package RefrigeratorV;
import java.util.*;

public class Refrigerator {
	int size;
	ArrayList<String> things;
	public Refrigerator(int size){
		this.size = size;
		things = new ArrayList<String>();
	}
	public void put(String stuff) throws FullException{
		if(things.size() == size){
			throw new FullException();
		}
		else{
			things.add(stuff);
		}
	}
	public String takeOut(String stuff){
		if(things.contains(stuff)){
			things.remove(stuff);
			return stuff;
		}
		else{
			return null;	
		}
	}
	public String toString(){
		String inRefrigerator = "";
		for(String item : things){
			inRefrigerator += item + " ";
		}
		return inRefrigerator;
	}
}